A set of vignettes for teaching, using AlphaSimR

# Installation instruction

install.packages(pkg = "devtools")

x = "hickeyjohnteam/AlphaLearn"

devtools::install_bitbucket(repo = x)
