% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/RunAlphaSoftware.R
\name{ocs}
\alias{ocs}
\title{Optimal Contribution Selection}
\usage{
ocs(pop, nCrosses, nFemalesMax = NULL, minFemaleContribution = NULL,
  maxFemaleContribution = NULL, equalizeFemaleContributions = NULL,
  nMalesMax = NULL, minMaleContribution = NULL,
  maxMaleContribution = NULL, equalizeMaleContributions = NULL,
  targetDegree = NULL, targetCoancestryRate = NULL, nProgenyPerCross, use)
}
\arguments{
\item{pop}{population}

\item{nCrosses}{number of matings/crosses}

\item{nFemalesMax}{maximum number of females}

\item{minFemaleContribution}{minimum number of matings/crosses per female}

\item{maxFemaleContribution}{maximum number of matings/crosses per female}

\item{nMalesMax}{maximum number of   males}

\item{minMaleContribution}{minimum number of matings/crosses per male}

\item{maxMaleContribution}{maximum number of matings/crosses per male}

\item{targetDegree}{targeted trigonometric degrees between genetic gain and group coancestry}

\item{targetCoancestryRate}{targeted rate of group coancestry}

\item{nProgenyPerCross}{number of progeny per mating/cross}

\item{use}{character specifiying, which type of criterion to use, either "pheno", "ebv", or "gv"}
}
\description{
Perform Optimal Contribution Selection via AlphaMate
.
}
