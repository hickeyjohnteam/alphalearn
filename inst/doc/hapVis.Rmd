---
title: "Haplotype visualisation"
output: rmarkdown::html_document
vignette: >
  %\VignetteIndexEntry{Haplotype visualisation}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## Introduction

In this vignette, we demonstrate visualisation of haplotype inheritance for an individual. We demonstrate this with the true and inferred inheritance process. The true inheritance process is based on data produced by AlphaSimR. The inferred inheritance process is based on output of an imputation algorithm. Here we used AlphaImpute, the development version 2.0, to infer founder haplotype of origin of each allele for each individual in the population. Where the algorithm was unable to assign origin to an allele, we assigned the value of -1.

The function to visualise haplotype inheritance is "hapVisForId(id, maxGens = 2)". This function creates a plot of the haplotypes for the individual *id* and of it's most recent ancestors. The number of generations back the algorithm goes is determined by *maxGens*.

We first load the course package.

```{r}
library(AlphaLearn)
```

## True inheritance process

Now we simulate a cattle population over four generations with 200 individuals per generation that are progeny of 10 sires and 250 dams. Parents are selected based on phenotype. We simulate just one chromosome here as this suffices for the purpose of this vignette. The chromosome has 1000 loci.

```{r}
# Constants
nHD = 1000
nInd = 200
# Ancestral haplotypes
founderPop = runMacs(nInd=nInd, nChr=1, segSites=nHD, species = "CATTLE")
# Simulation parameters
SP = SimParam$new(founderPop)
SP$addTraitA(nQtlPerChr=nHD, mean=0, var=1)
SP$addSnpChip(nSnpPerChr=nHD)
SP$setGender("yes_sys")
SP$setTrackPed(isTrackPed = TRUE)
SP$setTrackRec(isTrackRec = TRUE)
# Population over generations
pop = newPop(founderPop)
nCycles = 4
for(cycle in 1:nCycles){
    start = nInd*(cycle-1)+1
    end = nInd*cycle
    lastGen = pop[start:end]
    newPop = selectCross(pop=lastGen, nFemale=nInd/2, nMale=10, use="gv", nCrosses=nInd)
    pop = c(pop, newPop)
}
```

Here we prepare required data for plotting.

```{r}
trueDataForHapVis = getTrueDataForHapVis(pop)
```

We can now plot inheritance of individual's haplotypes.

Examples:

```{r, fig.width=7, fig.height=7}
# An individual with one generation of ancestors
hapVisForId(trueDataForHapVis, 201)
```

```{r, fig.width=7, fig.height=7}
# An individual with several generations of ancestors
hapVisForId(trueDataForHapVis, 801)
```

```{r, fig.width=7, fig.height=7}
# An individual with several generations of ancestors, but only display 2 ancestral generations
hapVisForId(trueDataForHapVis, 801, maxGens=2)
```

## Inferred inheritance process

For this part we have previously ran AlphaSimR to simulate an example data, then ran AlphaImpute to infer inheritance of haplotypes, and saved outputs into the package. Below we load these outputs, make a list of them, and plot inferred inheritance process with underlying true haplotype information - available due to simulated data. 

```{r}
data(origins)
data(pedigree)
data(truePhase)
inferredDataForHapVis = list(origins=origins, pedigree=pedigree, truePhase=truePhase)
```

Examples:
```{r, fig.width=7, fig.height=7}
hapVisForId(inferredDataForHapVis, 204, maxGens = 2)
```

```{r, fig.width=7, fig.height=7}
hapVisForId(inferredDataForHapVis, 900, maxGens = 2)
```

```{r, fig.width=7, fig.height=7}
hapVisForId(inferredDataForHapVis, 930, maxGens = 3)
```
