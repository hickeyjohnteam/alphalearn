---
title: "AlphaSimR Introduction"
output: rmarkdown::html_document
bibliography: bibliography.bib
vignette: >
  %\VignetteIndexEntry{AlphaSimR Introduction}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## Introduction

AlphaSimR is the successor to the AlphaSim software for breeding program simulation [@AlphaSim]. It combines the features of its predecessor with the R software environment to create an easy to use software platform for simulating complex plant and animal breeding programs.

To use the package, we must first load it.

```{r}
library(AlphaSimR)
```

There is no definitive way to construct a simulation in AlphaSimR. This is an intentional aspect of the AlphaSimR's design, because it frees the user from being constrained by a limited set of predefined simulation structures. However, most simulations will follow a general structure consisting of the following three steps:

1. Create Founder Haplotypes
2. Set Simulation Parameters
3. Simulate the Breeding Program

In this practicum we will show an example for each of these steps with code for an example breeding program simulation. We will then conclude the example with code for plotting the results using the ggplot2 package.

## Example Breeding Program

The example breeding program presented here is based on the "historical breeding" portion of the simulation performed by Jenko et al. [-@Jenko15]. The simulation models 20 discrete generations of selection in a simplified animal breeding program. Each generation consists of 1000 animals, of which 500 are male and 500 are female. In each generation the best 25 males are selected on the basis of their genetic value for a single trait. The selected males are mated with the females to produce 1000 replacement animals.

## Create Founder Haplotypes

The first step in the simulation is to create a set of founder haplotypes. These founder haplotypes will form the genomes of the animals in the first generation of the simulation. Thus, we need to create enough haplotypes to form 1000 animals. We also want to model each animal as having 10 chromosome pairs with 1000 segregating sites per chromosome to serve as QTL controlling the trait under selection. To create the necessary haplotypes, we will use the `runMacs` function. The `runMacs` function is a wrapper for the MaCS software, a coalescent simulation program distributed with AlphaSimR [@MaCS]. The appropriate `runMacs` call for meeting the previously described conditions is given below.

```{r}
founderPop = runMacs(nInd=1000, nChr=10, segSites=1000)
```

## Set Simulation Parameters

The second step in the simulation is to define the global simulation parameters. We will do this in three small steps. The first of these steps is to initialize an object containing the simulation parameters. The object is initialized with the founder haplotypes created above and the code for doing this is given below.

```{r}
SP = SimParam$new(founderPop)
```

Note that the output of this function is an object of class `SimParam` and that we have called it `SP`. We recommend always saving your output as `SP`. This is because many AlphaSimR functions take an argument called "simParam" and they have a default value of `NULL` for this argument. If you leave this value as `NULL`, the functions will search your global environment for an object called `SP` and use it as the function's argument. Thus, if you save your `SimParam` output as `SP`, you won't have to specify a value for "simParam" arguments.

Our next step is to define the trait used for selection. We do this below by adding an additive trait controlled by 1000 QTL per chromosome.

```{r}
SP$addTraitA(nQtlPerChr=1000)
```

The final step is to define how gender is determined. We define gender as systematically determined in the code below. This means populations with an even number of animals will always have equal numbers of males and females.

```{r}
SP$setGender("yes_sys")
# SP$setVarE(h2=0.2)
```

## Model the Breeding Program

We are now ready to start modeling the breeding program. When you do this, you need to think about what data you'd like to collect. Here we want to measure the change in both mean and variance over time. We will put this data in a `data.frame` so that it's easy to plot with `ggplot` later.

```{r}
output = data.frame(cycle=0:20,
                    mean=numeric(21),
                    var=numeric(21))
```

We now need to generate the initial population of animals. This step will take the haplotypes from `founderPop` and the information in `SP` to create an object of `Pop-class`. A `Pop-class` object represents a population of individuals and is one of the most important units in AlphaSimR. This is because many functions will use such objects as an argument. Note that the `Pop-class` can also be used "directly". For example, you can pull individuals out to form new (sub-)populations using `[]` and you can merge populations together using `c()`. You won't be doing any of this here, but these functions are useful if you want to perform a task in AlphaSimR that doesn't have a built in function.
This is one of the features that makes AlphaSimR flexible.

```{r}
pop = newPop(founderPop)
```

Now we want to start recording the mean and variance in the population by filling in the values for cycle zero.

```{r}
output$mean[1] = meanG(pop)
output$var[1] = varG(pop)
```

The next step is to model the 20 cycles of selection and mating. AlphaSimR has a host of functions for selection and mating. Here we will use the function `selectCross` that selects a number of males and females and mates them. The easiest way to do this is by writing a loop in R. In this loop, we overwrite `pop` in each cycle, because this is the most efficient use of memory and we have no need for saving each population.

```{r}
for(cycle in 1:20){
  # pop = setPheno(pop)
  pop = selectCross(pop=pop, nFemale=500, nMale=25,
                    use="gv", nCrosses=1000)
  output$mean[cycle+1] = meanG(pop)
  output$var[cycle+1] = varG(pop)
}
```

## Plot Results

This section provides code for creating plots for both the mean and variance using the `ggplot2`. This task can also be done with `plot` function in R if you don't want to use `ggplot2`.

```{r}
library(ggplot2)
```

```{r}
ggplot(output,aes(x=cycle,y=mean))+
  geom_line()+
  theme_bw()+
  xlab("Breeding Cycle")+
  ylab("Genetic Gain")
```

```{r}
ggplot(output,aes(x=cycle,y=var))+
  geom_line()+
  theme_bw()+
  xlab("Breeding Cycle")+
  ylab("Genetic Variance")+
  expand_limits(y=0)
```

## The sky's the limit

If we wanted to save the populations for some reason, the loop could be rewritten to use a list of `Pop-class` objects as shown below. This is a minor modification, but it shows that with AlphaSimR you have lots of flexiblity and what you do with it is limited only by your ability to express established or new breeding actions with R code!

```{r, eval=FALSE}
pops = vector("list", 21)
pops[[1]] = newPop(founderPop)
for(cycle in 1:20){
  pops[[cycle+1]] = selectCross(pop=pops[[cycle]], nFemale=500, nMale=25,
                                use="gv", nCrosses=1000)
}
output$mean = sapply(pops, meanG)
output$var = sapply(pops, varG)
```

## Challenge Problems

1. Run the simulation with different selection intensities on males, instead of 25 males select, say, 5 and 50 males.
2. Change the simulation to model selection on a phenotype with a narrow-sense heritability of 0.2. To do this, uncomment `SP$setVarE(h2=0.2)` in the simulation parameters, phenotype individuals prior to selection by uncommenting `pop = setPheno(pop)`, and change `use="gv"` to `use="pheno"` in `selectCross` .
3. Run 10 replications of the simulation with different scenarios. Make a plot with the average genetic gain over replications by scenario.

When simulating different scenarios or replicates of the same scenario, we find it useful to combine results and contrast them in a plot. You might achive this with a code like shown below.
```{r, eval=FALSE}
# 0) Generate founder haplotypes and set parameters
# Setup code START
# ...
# Setup code END

# 1) Run baseline scenario and store results
# Baseline code START
# ...
# Baseline code STOP
output$scenario = "Baseline"
outputAll = output

# 2) Run scenario 1 and store results
# Scenario 1 code START
# ...
# Scenario 1 code STOP
output$scenario = "Scenario1"
outputAll = rbind(outputAll, output)

# 3) Run scenario 2 and store results
# Scenario 2 code START
# ...
# Scenario 2 code STOP
output$scenario = "Scenario2"
outputAll = rbind(outputAll, output)

# Combine and plot
ggplot(outputAll,aes(x=cycle,y=mean,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding Cycle")+
  ylab("Genetic Gain")

ggplot(outputAll,aes(x=cycle,y=var,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding Cycle")+
  ylab("Genetic Variance")+
  expand_limits(y=0)
```

For generating simulation replicates and summarising results you might use a code like shown below.
```{r, eval=FALSE}
install.packages("tidyverse")
library(tidyverse)
nRep=2

for(rep in 1:nRep){
  # 0) Generate founder haplotypes and set parameters
  # Setup code START
  # ...
  # Setup code END

  # 1) Run baseline scenario and store results
  # Baseline code START
  # ...
  # Baseline code END
  output$scenario = "Baseline"
  output$rep = rep
  if(rep==1) {
    outputAll = output 
  } else {
    outputAll = rbind(outputAll, output)
  }
  
  # 2) Run scenario 1 and store results
  # Scenario 1 code START
  # ...
  # Scenario 1 code END
  output$scenario = "Scenario 1"
  output$rep = rep
  outputAll = rbind(outputAll, output)
  
  # 3) Run scenario 2 and store results
  # Scenario 2 code START
  # ...
  # Scenario 2 code END
  output$scenario = "Scenario 2"
  output$rep = rep
  outputAll = rbind(outputAll, output)
}

# Combine, summarise, and plot
outputAllSummary = outputAll %>%
  group_by(scenario, cycle) %>%
  summarise(meanGain = mean(mean))
ggplot(outputAllSummary,aes(x=cycle,y=meanGain,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding Cycle")+
  ylab("Genetic Gain")
```

## References
