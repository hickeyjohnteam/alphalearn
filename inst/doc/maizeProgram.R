## ------------------------------------------------------------------------
library(AlphaLearn)

## ---- eval=FALSE, include=TRUE-------------------------------------------
#  PkgDir = file.path(system.file(package="AlphaLearn"), "doc/maizeProgram")

## ---- eval=TRUE, include=FALSE-------------------------------------------
# A hack to get to scripts when compiling vignette
PkgDir = "maizeProgram"

## ------------------------------------------------------------------------
# Load global parameters
source(file.path(PkgDir, "GlobalParameters.R"))

nReps = 1 #Number of replications for experiment
burninYears = 20
futureYears = 20
# Initialize variables for results
hybridCorr = inbredMean = hybridMean = inbredVar = hybridVar =
  rep(NA_real_,burninYears+futureYears)
output = list(inbredMean=NULL,
              inbredVar=NULL,
              hybridMean=NULL,
              hybridVar=NULL,
              hybridCorr=NULL)
# Save results
saveRDS(output,"Scenario1.rds")
saveRDS(output,"Scenario2.rds")

# Loop through replications
for(rep in 1:nReps){
  # Create initial parents and set testers and hybrid parents
  source(file.path(PkgDir, "CreateParents.R"))

  # Fill breeding pipeline with unique individuals from initial parents
  source(file.path(PkgDir, "FillPipeline.R"))

  # p-values for GxY effects
  P = runif(burninYears+futureYears)

  # Cycle years
  for(year in 1:burninYears){ #Change to any number of desired years
    cat("Working on year:",year,"\n")
    p = P[year]

    source(file.path(PkgDir, "UpdateParents.R")) #Pick new inbred parents
    source(file.path(PkgDir, "UpdateTesters.R")) #Pick new testers and hybrid parents
    source(file.path(PkgDir, "AdvanceYear.R")) #Advances yield trials by a year

    # Update results
    inbredMean[year] = (meanG(MaleParents)+meanG(FemaleParents))/2
    inbredVar[year] = (varG(MaleParents)+varG(FemaleParents))/2

    tmp = hybridCross(FemaleParents,MaleParents)
    hybridMean[year] = meanG(tmp)
    hybridVar[year] = varG(tmp)

    tmp = calcGCA(tmp,use="gv")
    hybridCorr[year] = cor(c(tmp$GCAf[,2],tmp$GCAm[,2]),
                           c(FemaleParents@gv[,1],MaleParents@gv[,1]))
    rm(tmp)
  }
  save.image("tmp.RData")

  #Scenario 1
  cat("Working on Scenario 1\n")
  for(year in (burninYears+1):(burninYears+futureYears)){
    cat("Working on year:",year,"\n")
    p = P[year]

    source(file.path(PkgDir, "UpdateParents.R")) #Pick new inbred parents
    source(file.path(PkgDir, "UpdateTesters.R")) #Pick new testers and hybrid parents
    source(file.path(PkgDir, "AdvanceYear.R")) #Advances yield trials by a year

    # Update results
    inbredMean[year] = (meanG(MaleParents)+meanG(FemaleParents))/2
    inbredVar[year] = (varG(MaleParents)+varG(FemaleParents))/2

    tmp = hybridCross(FemaleParents,MaleParents)
    hybridMean[year] = meanG(tmp)
    hybridVar[year] = varG(tmp)

    tmp = calcGCA(tmp,use="gv")
    hybridCorr[year] = cor(c(tmp$GCAf[,2],tmp$GCAm[,2]),
                           c(FemaleParents@gv[,1],MaleParents@gv[,1]))
    rm(tmp)
  }

  # Report results for scenario 1
  output = readRDS("Scenario1.rds")
  output = list(inbredMean=rbind(output$inbredMean,inbredMean),
                inbredVar=rbind(output$inbredVar,inbredVar),
                hybridMean=rbind(output$hybridMean,hybridMean),
                hybridVar=rbind(output$hybridVar,hybridVar),
                hybridCorr=rbind(output$hybridCorr,hybridCorr))
  saveRDS(output,"Scenario1.rds")

  # Scenario 2
  load("tmp.RData")
  cat("Working on Scenario 2\n")
  for(year in (burninYears+1):(burninYears+futureYears)){
    cat("Working on year:",year,"\n")
    p = P[year]

    source(file.path(PkgDir, "UpdateParents.R")) #Pick new inbred parents
    source(file.path(PkgDir, "UpdateTesters.R")) #Pick new testers and hybrid parents
    source(file.path(PkgDir, "AdvanceYearAlt.R")) #Advances yield trials by a year

    # Update results
    inbredMean[year] = (meanG(MaleParents)+meanG(FemaleParents))/2
    inbredVar[year] = (varG(MaleParents)+varG(FemaleParents))/2

    tmp = hybridCross(FemaleParents,MaleParents)
    hybridMean[year] = meanG(tmp)
    hybridVar[year] = varG(tmp)

    tmp = calcGCA(tmp,use="gv")
    hybridCorr[year] = cor(c(tmp$GCAf[,2],tmp$GCAm[,2]),
                           c(FemaleParents@gv[,1],MaleParents@gv[,1]))
    rm(tmp)
  }
  # Report results for scenario 2
  output = readRDS("Scenario2.rds")
  output = list(inbredMean=rbind(output$inbredMean,inbredMean),
                inbredVar=rbind(output$inbredVar,inbredVar),
                hybridMean=rbind(output$hybridMean,hybridMean),
                hybridVar=rbind(output$hybridVar,hybridVar),
                hybridCorr=rbind(output$hybridCorr,hybridCorr))
  saveRDS(output,"Scenario2.rds")
}

## ------------------------------------------------------------------------
sc1 = readRDS("Scenario1.rds")
sc2 = readRDS("Scenario2.rds")

## ------------------------------------------------------------------------
plot(-19:20,colMeans(sc2$inbredMean),type="l",col=2,
     main="Inbred Yield",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,colMeans(sc1$inbredMean))

## ------------------------------------------------------------------------
plot(-19:20,colMeans(sc2$hybridMean),type="l",col=2,
     main="Hybrid Yield",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,colMeans(sc1$hybridMean))

## ------------------------------------------------------------------------
plot(-19:20,sqrt(colMeans(sc2$inbredVar)),type="l",col=2,
     main="Inbred Standard Deviation",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,sqrt(colMeans(sc1$inbredVar)))

## ------------------------------------------------------------------------
plot(-19:20,sqrt(colMeans(sc2$hybridVar)),type="l",col=2,
     main="Hybrid Standard Deviation",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,sqrt(colMeans(sc1$hybridVar)))

## ------------------------------------------------------------------------
plot(-19:20,sqrt(colMeans(sc2$hybridCorr)),type="l",col=2,
     main="GCA vs per se",xlab="Year",ylab="Correlation")
lines(-19:20,sqrt(colMeans(sc1$hybridCorr)))

## ------------------------------------------------------------------------
hybridYearFn = function(yield){ #bu/ac
  y = yield/0.01593 #kg/ha
  year = (y-4434.1)/74+1920
  return(year)
}
inbredYearFn = function(yield){ #bu/ac
  y = yield/0.01593 #kg/ha
  year = (y-1404.1)/48.3+1920
  return(year)
}

## ------------------------------------------------------------------------
round(hybridYearFn(colMeans(sc2$hybridMean)),1)

## ------------------------------------------------------------------------
round(inbredYearFn(colMeans(sc2$inbredMean)),1)

