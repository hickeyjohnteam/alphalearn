---
title: "Breeder's Equation"
output: rmarkdown::html_document
vignette: >
  %\VignetteIndexEntry{Breeder's Equation}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## Introduction

The breeder's equation for response to selection per unit time is:

$\LARGE R=\frac{ih\sigma_a}{t}$

where $i$ is the standardized selection intensity, $h$ is the square root of the narrow-sense heritability, $\sigma_a$ is the additive genetic standard deviation, and $t$ is the generation interval.

We will be using the `breederEq` function to illustrate how changing the components of breeder's equation impacts a breeding program. The `breederEq` function performs a small simulation to compare a baseline plant breeding program to an alternative user defined breeding program. The baseline breeding program has the following features:

* Plants with 10 chromosomes
  + each chromosome has 1 Morgan genetic length
* Selection on a quantitative trait
  + 100 QTL per chromosome
  + Additive genetic variance of 1
  + 0.5 heritability
* Discrete generations
  + 1 generation per year
  + 1000 plants per generation
  + 100 selected plants selected per generation
  + Random mating between selected plants

First we must load the course package.

```{r}
library(AlphaLearn)
```

## Genetic variance

Here we show how you can double response to selection by doubling additive genetic standard deviation.

```{r}
breederEq(var=4)
```

## Generation interval

Here we show how response to selection can be increased by decreasing generation interval. Note that increasing generations per year to 2 from 1 means reducing the generation interval to half a year from 1 year.
```{r, eval=FALSE}
breederEq(genPerYear=2)
```

## Selection accuracy

Here we show how response to selection is effected by the accuracy of selection.

```{r, eval=FALSE}
breederEq(h2=1)
```

Remember that response to selection is a function of the square root of heritability. This means the expected change in genetic gain is:
```{r}
sqrt(1)/sqrt(0.5)
```

## Selection intensity

Standardized selection intensity can be calculated with the following function:
```{r}
i = function(p){
  dnorm(qnorm(1-p))/p
}
```

The baseline simulation has the following selection intensity:
```{r}
i(100/1000)
```

You can increase selection intensity by decreasing the number of selected individuals selected or increasing the number of selection candidates. Note that decreasing the number of selected individuals to 10 gives the same selection intensity as increasing the number of selection candidates to 10,000:
```{r}
i(10/1000)
i(100/10000)
```

However, these scenarios don't have equivalent effects on long-term genetic gain due to the effect of drift.
```{r, eval=FALSE}
breederEq(nSel=10)
breederEq(genSize=10000)
```

## Not in the breeder's equation

### Recombination rate

You can increase the recombination rate by increasing the genetic length of the chromosomes.
```{r, eval=FALSE}
breederEq(Morgan=10)
```

### Number of QTL

Changing the number of QTL per chromosome can change the long-term performance of the simulation. Remember that most quantitative genetics theory is based on the infinitesimal model, which assumes an infinite number of QTL per chromosome.
```{r, eval=FALSE}
breederEq(nQtl=10)
breederEq(nQtl=1000)
```
