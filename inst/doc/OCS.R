## ------------------------------------------------------------------------
library(AlphaLearn)

## ------------------------------------------------------------------------
# Simulate founder haplotypes
founderPop = runMacs(nInd=200, nChr=10, segSites=2000, species="CATTLE")

# Set simulation parameters
SP = SimParam$new(founderPop)
SP$addTraitAD(nQtlPerChr=1000, meanDD=0.01, varDD=0.001)
SP$addSnpChip(nSnpPerChr=1000)
SP$setGender("yes_sys")
SP$setVarE(h2=0.25)

# Burnin
nGenBurnIn = 10
pop = newPop(founderPop)
for(cycle in 1:nGenBurnIn){
  pop = setPheno(pop)
  pop = selectCross(pop=pop, nFemale=100, nMale=5,
                    use="pheno", nCrosses=100, nProgeny=2)
}
popBase = pop

## ------------------------------------------------------------------------
pop = popBase
nGen = 10
output = data.frame(cycle=0:nGen,
                    meanG=numeric(nGen+1),
                    varG=numeric(nGen+1),
                    varGenic=numeric(nGen+1))
output$meanG[1] = meanG(pop)
output$varG[1] = varG(pop)
output$varGenic[1] = genicVarG(pop)
for(cycle in 1:nGenBurnIn){
  pop = setPheno(pop)
  pop = selectCross(pop=pop, nFemale=100, nMale=5,
                    use="pheno", nCrosses=100, nProgeny=2)
  output$meanG[cycle+1] = meanG(pop)
  output$varG[cycle+1] = varG(pop)
  output$varGenic[cycle+1] = genicVarG(pop)
}
output$scenario = "TS"
outputAll = output

## ------------------------------------------------------------------------
library(ggplot2)

## ------------------------------------------------------------------------
ggplot(output,aes(x=cycle,y=meanG))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic mean")

## ------------------------------------------------------------------------
ggplot(output,aes(x=cycle,y=varG))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic variance")+
  expand_limits(y=0)

## ------------------------------------------------------------------------
ggplot(output,aes(x=cycle,y=varGenic))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genic variance")+
  expand_limits(y=0)

## ------------------------------------------------------------------------
ggplot(output,aes(x=sqrt(varG),y=meanG))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genetic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genetic standard deviation"))

## ------------------------------------------------------------------------
ggplot(output,aes(x=sqrt(varGenic),y=meanG))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genic standard deviation"))

## ------------------------------------------------------------------------
targetDegrees = c(1, 15, 30, 45)
for (targetDegree in targetDegrees) {
  cat("\ntargetDegree", targetDegree, "\n")
  pop = popBase
  output = data.frame(cycle=0:nGen,
                    meanG=numeric(nGen+1),
                    varG=numeric(nGen+1),
                    varGenic=numeric(nGen+1))
  output$meanG[1] = meanG(pop)
  output$varG[1] = varG(pop)
  output$varGenic[1] = genicVarG(pop)
  for(cycle in 1:nGen){
    cat("cycle", cycle)
    pop = setPheno(pop)
    pop = ocs(pop=pop, nCrosses=100, nProgenyPerCross=2,
              nFemalesMax=100, equalizeFemaleContributions=TRUE,
              minMaleContribution=5, maxMaleContribution=50,
              targetDegree=targetDegree,
              use="pheno")
    print(table(pop@father))
    output$meanG[cycle+1] = meanG(pop)
    output$varG[cycle+1] = varG(pop)
    output$varGenic[cycle+1] = genicVarG(pop)
  }  
  output$scenario = paste0("OCS", targetDegree)
  outputAll = rbind(outputAll, output)
}

## ------------------------------------------------------------------------
ggplot(outputAll,aes(x=cycle,y=meanG,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic mean")

## ------------------------------------------------------------------------
ggplot(outputAll,aes(x=cycle,y=varG,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic variance")+
  expand_limits(y=0)

## ------------------------------------------------------------------------
ggplot(outputAll,aes(x=cycle,y=varGenic,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genic variance")+
  expand_limits(y=0)

## ------------------------------------------------------------------------
ggplot(outputAll,aes(x=sqrt(varG),y=meanG,colour=scenario))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genetic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genetic standard deviation"))

## ------------------------------------------------------------------------
ggplot(outputAll,aes(x=sqrt(varGenic),y=meanG,colour=scenario))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genic standard deviation"))

## ------------------------------------------------------------------------
for (scenario in unique(outputAll$scenario)) {
  cat("Scenario:", scenario, "\n")
  output = outputAll[outputAll$scenario == scenario, ]
  output$meanGZ = (output$meanG - output$meanG[1]) / sqrt(output$varGenic[1])
  output$sdGenicZ = sqrt(output$varGenic / output$varGenic[1])
  fit = lm(meanGZ ~ sdGenicZ, data=output)
  cat("Efficiency:", -coef(fit)[2], "\n")
  fit = glm(varG ~ cycle, family=Gamma(link="log"), data=output)
  dC = 1 - exp(coef(fit)[2])
  cat("Genetic dC:", dC, "\n")
  Ne = 1/(2*dC)
  cat("Genetic Ne:", Ne, "\n")
  fit = glm(varGenic ~ cycle, family=Gamma(link="log"), data=output)
  dC = 1 - exp(coef(fit)[2])
  cat("Genic dC:", dC, "\n")
  Ne = 1/(2*dC)
  cat("Genic Ne:", Ne, "\n\n")
}

