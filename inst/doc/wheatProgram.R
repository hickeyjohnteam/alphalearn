## ------------------------------------------------------------------------
library(AlphaLearn)

## ---- eval=FALSE, include=TRUE-------------------------------------------
#  PkgDir = file.path(system.file(package="AlphaLearn"), "doc/wheatProgram")

## ---- eval=TRUE, include=FALSE-------------------------------------------
# A hack to get to scripts when compiling vignette
PkgDir = "wheatProgram"

## ------------------------------------------------------------------------
# Load global parameters
source(file.path(PkgDir, "GlobalParameters.R"))

# Create initial parents
source(file.path(PkgDir, "CreateParents.R"))

# Fill breeding pipeline with unique individuals from initial parents
source(file.path(PkgDir, "FillPipeline.R"))

# Initialize variables for results
dhMean = dhVar = numeric(20)

# Cycle years to make more advanced parents
for(year in 1:20){ #Change to any number of desired years
  cat("Working on year:",year,"\n")
  source(file.path(PkgDir, "UpdateParents.R")) #Pick parents
  source(file.path(PkgDir, "AdvanceYear.R")) #Advances yield trials by a year
  # Update results
  dhMean[year] = meanG(DH)
  dhVar[year] = varG(DH)
}

## ------------------------------------------------------------------------
plot(dhMean, type = "b", xlab="Year", ylab="Genetic mean of DH lines")

## ------------------------------------------------------------------------
plot(dhVar, type = "b", xlab="Year", ylab="Genetic variance of DH lines")

