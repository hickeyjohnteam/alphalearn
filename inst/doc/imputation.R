## ------------------------------------------------------------------------
library(AlphaLearn)

# ---- Create Founder Haplotypes ----

nHD = 1000
nLD = 10
nInd = 200
founderPop = runMacs(nInd=nInd, nChr=1, segSites=nHD, species = "CATTLE")

# ---- Set Simulation Parameters ----

SP = SimParam$new(founderPop)
SP$addTraitA(nQtlPerChr=nHD, mean=0, var=1)
SP$addSnpChip(nSnpPerChr=nHD)
SP$setVarE(h2=.4)

# Setting the simulation to use gender
# Gender is assigned systematically as male, female, male, ...
SP$setGender("yes_sys")

# ---- Simulate the Breeding Program ----

# Creating the initial population
getEBVs = function(pop, genotypes, subset = NULL) {
    pheno = as.matrix(pop@pheno)
    X = as.matrix(rep(1, length(pop@id)))
    if(!is.null(subset)) {
      pheno = as.matrix(pheno[subset,])
      X = as.matrix(X[subset,1])
      genotypes = genotypes[subset,]
    }
    MMEList = AlphaMME::solveRRBLUP(pheno, X, genotypes)
    estBV = genotypes %*% MMEList$u
}

getImputationStatistics = function(cycle, generation, genotypes, trueGenotypes, generations) {
    indexes = which(generations == generation)
    subGenotypes = genotypes[indexes,]
    subTrueGenotypes = trueGenotypes[indexes,]

    #Apply population mean to missing markers.
    subGenotypes = apply(subGenotypes, 2, function(line) {
        line[line == 9] = NA
        mean = mean(line, na.rm=T)
        if(is.na(mean)) mean = 0
        line[is.na(line)] = mean
        line
    })
    #Obtain mean imputation accuracy, measured as a correlation.
    vals = sapply(1:nrow(subGenotypes), function(index){
        cor(subGenotypes[index,], subTrueGenotypes[index,])

    })
    val = mean(vals)

    return(c(cycle, generation, val))
}

getAccuracyStatistics = function(class, generation, estBV, gv, generations) {
    #Subset EBVs for the last generation.
    indexes = which(generations == generation)
    return(c(class, generation, cor(estBV[indexes], gv[indexes])))
}

## ------------------------------------------------------------------------

pop = newPop(founderPop)

accuracyStatistics = data.frame()
imputationStatistics = data.frame()
generations = rep(1, length(pop@id))

nCycles = 5
for(cycle in 1:nCycles){
      
    ### Generate the genotypes:
    
    trueGenotypes = AlphaSimR::pullSnpGeno(pop, 1)
    
    maskedGenotypes = AlphaSimR::pullSnpGeno(pop, 1)
    ldMarkers = floor(seq(1, nHD, length.out=nLD))
    #Use all parents
    #hdIndividuals = unique(c(pop@mother, pop@father))
    #Use Father + first 200 individuals.
    hdIndividuals = unique(c(pop@father, pop@id[1:200]))
    #Use just fathers
    #hdIndividuals = unique(c(pop@father))
    hdIndividuals = setdiff(hdIndividuals, 0)
    ldIndexes = which(!(pop@id %in% hdIndividuals))
    maskedGenotypes[ldIndexes, -ldMarkers] = 9 
    ldGenotypes = trueGenotypes[,ldMarkers]
    imputedGenotypes = maskedGenotypes


    ### Perform Imputation

    if(cycle > 1) {
        pedigree = data.frame(pop@id, pop@father, pop@mother, stringsAsFactors = F)
#        imputedGenotypes = AlphaLearn:::runAlphaImpute(pedigree, maskedGenotypes)$genotypes
        imputedGenotypes = AlphaLearn:::runAlphaImpute(pedigree, maskedGenotypes)$gdosages

        ids = imputedGenotypes[,1]
        newLoci = sapply(pop@id, function(id) {
            which(ids == id)
        })
        imputedGenotypes = imputedGenotypes[newLoci,-1]
    }
    

    ### Generate summary data for each generation.

    for(gen in 1:cycle) {
        imputationStatistics = rbind(imputationStatistics, getImputationStatistics(cycle, gen, imputedGenotypes, trueGenotypes, generations))
    }

    
    ### Use the imputed genotypes to generate the EBVs and perform selection.

    if(cycle != nCycles) {
        pop@ebv = getEBVs(pop, imputedGenotypes)
        start = nInd*(cycle-1)+1
        end = nInd*cycle
        lastGen = pop[start:end]
        newPop =  selectCross(pop=lastGen, nFemale=nInd/2, nMale=10, use="ebv", nCrosses=nInd)
        pop = c(pop, newPop)
        generations = c(generations, rep(cycle+1, length(newPop@id))) 
    }
    

}
### Generate accuracy statistics for the final generation

ldEBVs = getEBVs(pop, ldGenotypes)
imputeEBVs = getEBVs(pop, imputedGenotypes)
hdEBVs = getEBVs(pop, trueGenotypes)
for(gen in 1:cycle) {
  accuracyStatistics = rbind(accuracyStatistics, getAccuracyStatistics(1, gen, ldEBVs, pop@gv, generations))
  accuracyStatistics = rbind(accuracyStatistics, getAccuracyStatistics(2, gen, imputeEBVs, pop@gv, generations))
  accuracyStatistics = rbind(accuracyStatistics, getAccuracyStatistics(3, gen, hdEBVs, pop@gv, generations))
}


## ----fig.width=6, fig.height=4-------------------------------------------

#Create summary figures

colors = colorRampPalette(c("lightblue", "darkblue"))(5)
colnames(imputationStatistics) = c("cycle", "gen", "val")
plot(c(), xlim = c(1,5), ylim = c(0, 1), xlab = "Generation", ylab = "Imputation accuracy", main = "Imputation accuracy with increasing data")
for(cycle in 1:5) {
    subData = imputationStatistics[imputationStatistics$cycle == cycle,]
    lines(subData$gen, subData$val, col = colors[cycle], type="b")

}
legend("bottomleft", legend=c(1, 2, 3, 4, 5), fill = colors, bty="n", title = "Number of generations")


## ----fig.width=6, fig.height=4-------------------------------------------

colors = c("blue", "green", "red")
colnames(accuracyStatistics) = c("type", "gen", "val")
plot(c(), xlim = c(1,5), ylim = c(0, 1), xlab = "Generation", ylab = "GEBV Accuracy", main = "GEBV accuracy with 5 generations of LD and HD data")
for(type in 1:3) {
    subData = accuracyStatistics[accuracyStatistics$type == type,]
    lines(subData$gen, subData$val, col = colors[type], type = "b")

}
legend("bottomleft", c("LD data", "Imputed data", "HD data"), fill = colors, bty="n", title = "Genotype data")


## ----fig.width=6, fig.height=4-------------------------------------------
phenotypingCost = 50
ldArrayCost = 10
hdArrayCost = 60

ldAccuracy = accuracyStatistics[13,3]
imputeAccuracy = accuracyStatistics[14,3]
hdAccuracy = accuracyStatistics[15,3]


ldCost = nInd*phenotypingCost + nInd*ldArrayCost
imputeCost = nInd*phenotypingCost + nInd*ldArrayCost + 10*hdArrayCost
hdCost = nInd*phenotypingCost + nInd*hdArrayCost

accuracyPerCost = c(LD = ldAccuracy/ldCost, Imputed = imputeAccuracy/imputeCost, HD= hdAccuracy/hdCost)
#Normalize to the last value.
accuracyPerCost = accuracyPerCost/accuracyPerCost[3]


midpoints = barplot(accuracyPerCost, beside=TRUE, ylab = "Relative Response/Cost", ylim = c(0, max(accuracyPerCost)+.2))

text(midpoints, accuracyPerCost + .1, labels = round(accuracyPerCost,3))


