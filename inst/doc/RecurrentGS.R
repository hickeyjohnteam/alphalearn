## ------------------------------------------------------------------------
library(AlphaSimR)

## ------------------------------------------------------------------------
# Genome parameters
nChr = 10
nQtl = 200      
nSnp = 200


# Breeding program parameters
nFounders = 50
nParents = 50
nCrosses = 100       
nProgeny = 10        
breedingCycles = 10      


# Trait parameters                  
initMeanG = 10              
initVarG = 1              
h2 = 0.4


# Create founder population and initiate simulation
founderPop = runMacs(nInd=nFounders, nChr=nChr, segSites=nQtl+nSnp)

SP = SimParam$new(founderPop)

SP$addTraitA(nQtlPerChr=nQtl, mean=initMeanG, var=initVarG)

SP$setVarE(h2=h2)

SP$addSnpChip(nSnp)    # Number of SNPs per chromosome


output = data.frame(cycle=0:breedingCycles,
                    mean=numeric(breedingCycles+1), 
                    var=numeric(breedingCycles+1))

initPop = newPop(founderPop)

# Create initial population of 500 individuals by randomly crossing the founder genotypes
initPop = randCross(initPop, nCrosses=nCrosses, nProgeny = nProgeny)

## ------------------------------------------------------------------------
pop=initPop

output$mean[1] = meanG(pop)
output$var[1] = varG(pop)

gsAccuracy = data.frame(cycle=1:breedingCycles,
                        accuracy=numeric(breedingCycles))

# Run breeding program simulation
for(cycle in 1:breedingCycles){
  
  cat(paste0("Breeding cycle ", cycle,"\n"))
  
  pop = setPheno(pop)
  gp = genParam(pop)
  gsAccuracy[cycle,2]= (cor(pop@pheno,gp$bv))
  
  pop = selectCross(pop=pop, nInd = nParents, use="pheno", nCrosses=nCrosses, nProgeny = nProgeny)
  output$mean[cycle+1] = meanG(pop)
  output$var[cycle+1] = varG(pop)
}

output$scenario ="PheSel"
gsAccuracy$scenario = "PheSel"
outputAll = output
gsAccuracyAll = gsAccuracy

## ------------------------------------------------------------------------
pop=initPop

output = data.frame(cycle=0:breedingCycles,
                    mean=numeric(breedingCycles+1), 
                    var=numeric(breedingCycles+1))

gsAccuracy = data.frame(cycle=1:breedingCycles,
                        accuracy=numeric(breedingCycles))

output$mean[1] = meanG(pop)
output$var[1] = varG(pop)


# Run breeding program simulation
for(cycle in 1:breedingCycles){
  
  cat(paste0("Breeding cycle ", cycle,"\n"))
  
  gsTp = RRBLUP(pop)
  
  pop = setEBV(pop, gsTp)
  gp = genParam(pop)
  gsAccuracy[cycle,2]= (cor(pop@ebv,gp$bv))
  
  pop = selectInd(pop, nInd=nParents, use="ebv")
  pop = randCross(pop, nCrosses=nCrosses, nProgeny = nProgeny)
  
  output$mean[cycle+1] = meanG(pop)
  output$var[cycle+1] = varG(pop)
}

output$scenario= "GS"
gsAccuracy$scenario = "GS"
outputAll = rbind(outputAll, output)
gsAccuracyAll = rbind(gsAccuracyAll, gsAccuracy)

## ------------------------------------------------------------------------
pop=initPop

output = data.frame(cycle=0:breedingCycles,
                    mean=numeric(breedingCycles+1), 
                    var=numeric(breedingCycles+1))

gsAccuracy = data.frame(cycle=seq(0.5,breedingCycles,0.5),
                        accuracy=numeric(2*breedingCycles))

output$mean[1] = meanG(pop)
output$var[1] = varG(pop)


# Run breeding program simulation
for(cycle in 1:breedingCycles){
  
  cat(paste0("Breeding cycle ", cycle,"\n"))
  
  gsTp = RRBLUP(pop)
  
  
  # 1st selection cycle in one year
  
  # Estimated breeding values are calculated
  pop = setEBV(pop, gsTp)   
  gp = genParam(pop)
  gsAccuracy[2*cycle-1,2]= (cor(pop@ebv,gp$bv))
  
  pop = selectCross(pop=pop, nInd = nParents, use="ebv", nCrosses=nCrosses, nProgeny = nProgeny)
  
  
  # 2nd selection cycle in one year
  # The population deriving from this crossing will be used as TP
  
  pop = setEBV(pop, gsTp)
  gp = genParam(pop)
  gsAccuracy[2*cycle,2]= (cor(pop@ebv,gp$bv))
  
  pop = selectCross(pop=pop, nInd = nParents, use="ebv", nCrosses=nCrosses, nProgeny = nProgeny)
  
  output$mean[cycle+1] = meanG(pop)
  output$var[cycle+1] = varG(pop)
}

output$scenario= "GS2"
gsAccuracy$scenario = "GS2"
outputAll = rbind(outputAll, output)
gsAccuracyAll = rbind(gsAccuracyAll, gsAccuracy)


## ------------------------------------------------------------------------
ggplot(outputAll,aes(x=cycle,y=mean,colour=scenario))+
  geom_line()+
  theme_bw()+
  scale_x_continuous(breaks=seq(0,10,1))+
  xlab("Breeding cycle")+
  ylab("Genetic mean")

ggplot(outputAll,aes(x=cycle,y=var,colour=scenario))+
  geom_line()+
  theme_bw()+
  scale_x_continuous(breaks=seq(0,10,1))+
  xlab("Breeding cycle")+
  ylab("Genetic variance")

ggplot(gsAccuracyAll,aes(x=cycle,y=accuracy,colour=scenario))+
  geom_line()+
  theme_bw()+
  scale_x_continuous(breaks=seq(0,10,1))+
  xlab("Breeding cycle")+
  ylab("Selection accuracy")
