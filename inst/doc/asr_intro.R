## ------------------------------------------------------------------------
library(AlphaSimR)

## ------------------------------------------------------------------------
founderPop = runMacs(nInd=1000, nChr=10, segSites=1000)

## ------------------------------------------------------------------------
SP = SimParam$new(founderPop)

## ------------------------------------------------------------------------
SP$addTraitA(nQtlPerChr=1000)

## ------------------------------------------------------------------------
SP$setGender("yes_sys")
# SP$setVarE(h2=0.2)

## ------------------------------------------------------------------------
output = data.frame(cycle=0:20,
                    mean=numeric(21),
                    var=numeric(21))

## ------------------------------------------------------------------------
pop = newPop(founderPop)

## ------------------------------------------------------------------------
output$mean[1] = meanG(pop)
output$var[1] = varG(pop)

## ------------------------------------------------------------------------
for(cycle in 1:20){
  # pop = setPheno(pop)
  pop = selectCross(pop=pop, nFemale=500, nMale=25,
                    use="gv", nCrosses=1000)
  output$mean[cycle+1] = meanG(pop)
  output$var[cycle+1] = varG(pop)
}

## ------------------------------------------------------------------------
library(ggplot2)

## ------------------------------------------------------------------------
ggplot(output,aes(x=cycle,y=mean))+
  geom_line()+
  theme_bw()+
  xlab("Breeding Cycle")+
  ylab("Genetic Gain")

## ------------------------------------------------------------------------
ggplot(output,aes(x=cycle,y=var))+
  geom_line()+
  theme_bw()+
  xlab("Breeding Cycle")+
  ylab("Genetic Variance")+
  expand_limits(y=0)

## ---- eval=FALSE---------------------------------------------------------
#  pops = vector("list", 21)
#  pops[[1]] = newPop(founderPop)
#  for(cycle in 1:20){
#    pops[[cycle+1]] = selectCross(pop=pops[[cycle]], nFemale=500, nMale=25,
#                                  use="gv", nCrosses=1000)
#  }
#  output$mean = sapply(pops, meanG)
#  output$var = sapply(pops, varG)

## ---- eval=FALSE---------------------------------------------------------
#  # 0) Generate founder haplotypes and set parameters
#  # Setup code START
#  # ...
#  # Setup code END
#  
#  # 1) Run baseline scenario and store results
#  # Baseline code START
#  # ...
#  # Baseline code STOP
#  output$scenario = "Baseline"
#  outputAll = output
#  
#  # 2) Run scenario 1 and store results
#  # Scenario 1 code START
#  # ...
#  # Scenario 1 code STOP
#  output$scenario = "Scenario1"
#  outputAll = rbind(outputAll, output)
#  
#  # 3) Run scenario 2 and store results
#  # Scenario 2 code START
#  # ...
#  # Scenario 2 code STOP
#  output$scenario = "Scenario2"
#  outputAll = rbind(outputAll, output)
#  
#  # Combine and plot
#  ggplot(outputAll,aes(x=cycle,y=mean,colour=scenario))+
#    geom_line()+
#    theme_bw()+
#    xlab("Breeding Cycle")+
#    ylab("Genetic Gain")
#  
#  ggplot(outputAll,aes(x=cycle,y=var,colour=scenario))+
#    geom_line()+
#    theme_bw()+
#    xlab("Breeding Cycle")+
#    ylab("Genetic Variance")+
#    expand_limits(y=0)

## ---- eval=FALSE---------------------------------------------------------
#  install.packages("tidyverse")
#  library(tidyverse)
#  nRep=2
#  
#  for(rep in 1:nRep){
#    # 0) Generate founder haplotypes and set parameters
#    # Setup code START
#    # ...
#    # Setup code END
#  
#    # 1) Run baseline scenario and store results
#    # Baseline code START
#    # ...
#    # Baseline code END
#    output$scenario = "Baseline"
#    output$rep = rep
#    if(rep==1) {
#      outputAll = output
#    } else {
#      outputAll = rbind(outputAll, output)
#    }
#  
#    # 2) Run scenario 1 and store results
#    # Scenario 1 code START
#    # ...
#    # Scenario 1 code END
#    output$scenario = "Scenario 1"
#    output$rep = rep
#    outputAll = rbind(outputAll, output)
#  
#    # 3) Run scenario 2 and store results
#    # Scenario 2 code START
#    # ...
#    # Scenario 2 code END
#    output$scenario = "Scenario 2"
#    output$rep = rep
#    outputAll = rbind(outputAll, output)
#  }
#  
#  # Combine, summarise, and plot
#  outputAllSummary = outputAll %>%
#    group_by(scenario, cycle) %>%
#    summarise(meanGain = mean(mean))
#  ggplot(outputAllSummary,aes(x=cycle,y=meanGain,colour=scenario))+
#    geom_line()+
#    theme_bw()+
#    xlab("Breeding Cycle")+
#    ylab("Genetic Gain")

