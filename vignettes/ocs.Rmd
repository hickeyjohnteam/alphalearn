---
title: "Optimal Contribution Selection"
output: rmarkdown::html_document
bibliography: bibliography.bib
vignette: >
  %\VignetteIndexEntry{Optimal Contribution Selection}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## Introduction

In this vignette we demonstrate the use of Optimal Contribution Selection (OCS) to balance selection and maintenance of genetic diversity in a simulated population. We use AlphaMate [-@Gorjanc18] to run optimisation with an evolutionary algorithm. 

The simulated population is small (200 individuals) and selected over 10 generations to quickly explore long-term trends with different balance between selection and maintenance. Every generation we select all 100 females and 5 males in the baseline scenario of truncation selection. Then we run optimal contribution selection, where we also select all 100 females, but we optimise the selection and contributions of males acording to the desired balance between selection criterion and group coancestry.

We first load the package.

```{r}
library(AlphaLearn)
```

## Burn-in

Then we run breeding program for 10 generations to obtain a snapshot of a real-like breeding program.

```{r}
# Simulate founder haplotypes
founderPop = runMacs(nInd=200, nChr=10, segSites=2000, species="CATTLE")

# Set simulation parameters
SP = SimParam$new(founderPop)
SP$addTraitAD(nQtlPerChr=1000, meanDD=0.01, varDD=0.001)
SP$addSnpChip(nSnpPerChr=1000)
SP$setGender("yes_sys")
SP$setVarE(h2=0.25)

# Burnin
nGenBurnIn = 10
pop = newPop(founderPop)
for(cycle in 1:nGenBurnIn){
  pop = setPheno(pop)
  pop = selectCross(pop=pop, nFemale=100, nMale=5,
                    use="pheno", nCrosses=100, nProgeny=2)
}
popBase = pop
```

## Truncation selection scenario

We continue with the same selection scheme (truncation selection), but now monitor genetic mean and variances.

```{r}
pop = popBase
nGen = 10
output = data.frame(cycle=0:nGen,
                    meanG=numeric(nGen+1),
                    varG=numeric(nGen+1),
                    varGenic=numeric(nGen+1))
output$meanG[1] = meanG(pop)
output$varG[1] = varG(pop)
output$varGenic[1] = genicVarG(pop)
for(cycle in 1:nGenBurnIn){
  pop = setPheno(pop)
  pop = selectCross(pop=pop, nFemale=100, nMale=5,
                    use="pheno", nCrosses=100, nProgeny=2)
  output$meanG[cycle+1] = meanG(pop)
  output$varG[cycle+1] = varG(pop)
  output$varGenic[cycle+1] = genicVarG(pop)
}
output$scenario = "TS"
outputAll = output
```

```{r}
library(ggplot2)
```

```{r}
ggplot(output,aes(x=cycle,y=meanG))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic mean")
```

```{r}
ggplot(output,aes(x=cycle,y=varG))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic variance")+
  expand_limits(y=0)
```

```{r}
ggplot(output,aes(x=cycle,y=varGenic))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genic variance")+
  expand_limits(y=0)
```

```{r}
ggplot(output,aes(x=sqrt(varG),y=meanG))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genetic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genetic standard deviation"))
```

```{r}
ggplot(output,aes(x=sqrt(varGenic),y=meanG))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genic standard deviation"))
```

## Optimal contribution selection

Now we run the same kind of simulation, but with optimising selection of males and their contributions. We perform this optimisation "trigonometric degrees". We assume that each male can be used at most for 50 matings. We also limit minimal use to 5 matings to avoid selecting a male for a too small number of crosses. 

```{r}
targetDegrees = c(1, 15, 30, 45)
for (targetDegree in targetDegrees) {
  cat("\ntargetDegree", targetDegree, "\n")
  pop = popBase
  output = data.frame(cycle=0:nGen,
                    meanG=numeric(nGen+1),
                    varG=numeric(nGen+1),
                    varGenic=numeric(nGen+1))
  output$meanG[1] = meanG(pop)
  output$varG[1] = varG(pop)
  output$varGenic[1] = genicVarG(pop)
  for(cycle in 1:nGen){
    cat("cycle", cycle)
    pop = setPheno(pop)
    pop = ocs(pop=pop, nCrosses=100, nProgenyPerCross=2,
              nFemalesMax=100, equalizeFemaleContributions=TRUE,
              minMaleContribution=5, maxMaleContribution=50,
              targetDegree=targetDegree,
              use="pheno")
    print(table(pop@father))
    output$meanG[cycle+1] = meanG(pop)
    output$varG[cycle+1] = varG(pop)
    output$varGenic[cycle+1] = genicVarG(pop)
  }  
  output$scenario = paste0("OCS", targetDegree)
  outputAll = rbind(outputAll, output)
}
```

Plot results

```{r}
ggplot(outputAll,aes(x=cycle,y=meanG,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic mean")
```

```{r}
ggplot(outputAll,aes(x=cycle,y=varG,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genetic variance")+
  expand_limits(y=0)
```

```{r}
ggplot(outputAll,aes(x=cycle,y=varGenic,colour=scenario))+
  geom_line()+
  theme_bw()+
  xlab("Breeding cycle")+
  ylab("Genic variance")+
  expand_limits(y=0)
```

```{r}
ggplot(outputAll,aes(x=sqrt(varG),y=meanG,colour=scenario))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genetic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genetic standard deviation"))
```

```{r}
ggplot(outputAll,aes(x=sqrt(varGenic),y=meanG,colour=scenario))+
  geom_path()+
  geom_point()+
  theme_bw()+
  ylab("Genetic mean")+
  xlab("Genic standard deviation")+
  scale_x_reverse(sec.axis=sec_axis(trans=~1-.,
                                    name="Converted/Lost genic standard deviation"))
```


Estimate the rate of coancestry and effective population size:

```{r}
for (scenario in unique(outputAll$scenario)) {
  cat("Scenario:", scenario, "\n")
  output = outputAll[outputAll$scenario == scenario, ]
  output$meanGZ = (output$meanG - output$meanG[1]) / sqrt(output$varGenic[1])
  output$sdGenicZ = sqrt(output$varGenic / output$varGenic[1])
  fit = lm(meanGZ ~ sdGenicZ, data=output)
  cat("Efficiency:", -coef(fit)[2], "\n")
  fit = glm(varG ~ cycle, family=Gamma(link="log"), data=output)
  dC = 1 - exp(coef(fit)[2])
  cat("Genetic dC:", dC, "\n")
  Ne = 1/(2*dC)
  cat("Genetic Ne:", Ne, "\n")
  fit = glm(varGenic ~ cycle, family=Gamma(link="log"), data=output)
  dC = 1 - exp(coef(fit)[2])
  cat("Genic dC:", dC, "\n")
  Ne = 1/(2*dC)
  cat("Genic Ne:", Ne, "\n\n")
}
```

## Challenge Problems

1. Make a note of efficiency for each scenario and their order. Then rerun the simulation at least once to gain a sense how results could vary from replication to replication. Can you think why?
2. Increase the average dominance degree (`meanDD` argument in `SP$addTraitAD`) and rerun simulation. For example, try with value of 0.3. What happens?
3. Modify script to collect also additive genetic variance and variance due to dominance (use functions `varA`, `varD`, `genicVarA`, and `genicVarD`). Plot these variances against cycle.

## References
