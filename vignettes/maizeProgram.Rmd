---
title: "Maize breeding program"
output: rmarkdown::html_document
bibliography: bibliography.bib
vignette: >
  %\VignetteIndexEntry{Maize breeding program}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## Introduction

This vignette runs a set of scripts to simulate a whole maize breeding program with two scenarios. The baseline scenario evaluates DH lines on test-cross performance. The alternative scenarios switches evaluation of DH lines to per-se performance.

The simulation models 20 years of burn-in in a breeding using the test-cross scheme followed by 20 years of breeding with each of scenarios. The design of the breeding program is based on the breeding program on page 211 of "Essentials of Plant Breeding" [@BernardoBook]. The rationale for this comparison is based on a recommendation of Troyer and Wellin [-@Troyer09].

To run this simulation, run the code below. Note that the code below is sourcing other scripts for different breeding actions. The code finishes with a report of results in plots and to files called "Scenario1.rds" and "Scenario2.rds".

## Simulation

```{r}
library(AlphaLearn)
```

```{r, eval=FALSE, include=TRUE}
PkgDir = file.path(system.file(package="AlphaLearn"), "doc/maizeProgram")
```

```{r, eval=TRUE, include=FALSE}
# A hack to get to scripts when compiling vignette
PkgDir = "maizeProgram"
```

```{r}
# Load global parameters
source(file.path(PkgDir, "GlobalParameters.R"))

nReps = 1 #Number of replications for experiment
burninYears = 20
futureYears = 20
# Initialize variables for results
hybridCorr = inbredMean = hybridMean = inbredVar = hybridVar =
  rep(NA_real_,burninYears+futureYears)
output = list(inbredMean=NULL,
              inbredVar=NULL,
              hybridMean=NULL,
              hybridVar=NULL,
              hybridCorr=NULL)
# Save results
saveRDS(output,"Scenario1.rds")
saveRDS(output,"Scenario2.rds")

# Loop through replications
for(rep in 1:nReps){
  # Create initial parents and set testers and hybrid parents
  source(file.path(PkgDir, "CreateParents.R"))

  # Fill breeding pipeline with unique individuals from initial parents
  source(file.path(PkgDir, "FillPipeline.R"))

  # p-values for GxY effects
  P = runif(burninYears+futureYears)

  # Cycle years
  for(year in 1:burninYears){ #Change to any number of desired years
    cat("Working on year:",year,"\n")
    p = P[year]

    source(file.path(PkgDir, "UpdateParents.R")) #Pick new inbred parents
    source(file.path(PkgDir, "UpdateTesters.R")) #Pick new testers and hybrid parents
    source(file.path(PkgDir, "AdvanceYear.R")) #Advances yield trials by a year

    # Update results
    inbredMean[year] = (meanG(MaleParents)+meanG(FemaleParents))/2
    inbredVar[year] = (varG(MaleParents)+varG(FemaleParents))/2

    tmp = hybridCross(FemaleParents,MaleParents)
    hybridMean[year] = meanG(tmp)
    hybridVar[year] = varG(tmp)

    tmp = calcGCA(tmp,use="gv")
    hybridCorr[year] = cor(c(tmp$GCAf[,2],tmp$GCAm[,2]),
                           c(FemaleParents@gv[,1],MaleParents@gv[,1]))
    rm(tmp)
  }
  save.image("tmp.RData")

  #Scenario 1
  cat("Working on Scenario 1\n")
  for(year in (burninYears+1):(burninYears+futureYears)){
    cat("Working on year:",year,"\n")
    p = P[year]

    source(file.path(PkgDir, "UpdateParents.R")) #Pick new inbred parents
    source(file.path(PkgDir, "UpdateTesters.R")) #Pick new testers and hybrid parents
    source(file.path(PkgDir, "AdvanceYear.R")) #Advances yield trials by a year

    # Update results
    inbredMean[year] = (meanG(MaleParents)+meanG(FemaleParents))/2
    inbredVar[year] = (varG(MaleParents)+varG(FemaleParents))/2

    tmp = hybridCross(FemaleParents,MaleParents)
    hybridMean[year] = meanG(tmp)
    hybridVar[year] = varG(tmp)

    tmp = calcGCA(tmp,use="gv")
    hybridCorr[year] = cor(c(tmp$GCAf[,2],tmp$GCAm[,2]),
                           c(FemaleParents@gv[,1],MaleParents@gv[,1]))
    rm(tmp)
  }

  # Report results for scenario 1
  output = readRDS("Scenario1.rds")
  output = list(inbredMean=rbind(output$inbredMean,inbredMean),
                inbredVar=rbind(output$inbredVar,inbredVar),
                hybridMean=rbind(output$hybridMean,hybridMean),
                hybridVar=rbind(output$hybridVar,hybridVar),
                hybridCorr=rbind(output$hybridCorr,hybridCorr))
  saveRDS(output,"Scenario1.rds")

  # Scenario 2
  load("tmp.RData")
  cat("Working on Scenario 2\n")
  for(year in (burninYears+1):(burninYears+futureYears)){
    cat("Working on year:",year,"\n")
    p = P[year]

    source(file.path(PkgDir, "UpdateParents.R")) #Pick new inbred parents
    source(file.path(PkgDir, "UpdateTesters.R")) #Pick new testers and hybrid parents
    source(file.path(PkgDir, "AdvanceYearAlt.R")) #Advances yield trials by a year

    # Update results
    inbredMean[year] = (meanG(MaleParents)+meanG(FemaleParents))/2
    inbredVar[year] = (varG(MaleParents)+varG(FemaleParents))/2

    tmp = hybridCross(FemaleParents,MaleParents)
    hybridMean[year] = meanG(tmp)
    hybridVar[year] = varG(tmp)

    tmp = calcGCA(tmp,use="gv")
    hybridCorr[year] = cor(c(tmp$GCAf[,2],tmp$GCAm[,2]),
                           c(FemaleParents@gv[,1],MaleParents@gv[,1]))
    rm(tmp)
  }
  # Report results for scenario 2
  output = readRDS("Scenario2.rds")
  output = list(inbredMean=rbind(output$inbredMean,inbredMean),
                inbredVar=rbind(output$inbredVar,inbredVar),
                hybridMean=rbind(output$hybridMean,hybridMean),
                hybridVar=rbind(output$hybridVar,hybridVar),
                hybridCorr=rbind(output$hybridCorr,hybridCorr))
  saveRDS(output,"Scenario2.rds")
}
```

Read in results

```{r}
sc1 = readRDS("Scenario1.rds")
sc2 = readRDS("Scenario2.rds")
```

Plot results

Inbred Mean
```{r}
plot(-19:20,colMeans(sc2$inbredMean),type="l",col=2,
     main="Inbred Yield",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,colMeans(sc1$inbredMean))
```

Hybrid Mean

```{r}
plot(-19:20,colMeans(sc2$hybridMean),type="l",col=2,
     main="Hybrid Yield",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,colMeans(sc1$hybridMean))
```

Inbred Variance

```{r}
plot(-19:20,sqrt(colMeans(sc2$inbredVar)),type="l",col=2,
     main="Inbred Standard Deviation",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,sqrt(colMeans(sc1$inbredVar)))
```

Hybrid Variance

```{r}
plot(-19:20,sqrt(colMeans(sc2$hybridVar)),type="l",col=2,
     main="Hybrid Standard Deviation",xlab="Year",ylab="Yield (bu/ac)")
lines(-19:20,sqrt(colMeans(sc1$hybridVar)))
```

Hybrid Corr

```{r}
plot(-19:20,sqrt(colMeans(sc2$hybridCorr)),type="l",col=2,
     main="GCA vs per se",xlab="Year",ylab="Correlation")
lines(-19:20,sqrt(colMeans(sc1$hybridCorr)))
```

See how well long-term gain compares to published values. For this we write functions for matching regression lines in Troyer and Wellin [-@Troyer09].

```{r}
hybridYearFn = function(yield){ #bu/ac
  y = yield/0.01593 #kg/ha
  year = (y-4434.1)/74+1920
  return(year)
}
inbredYearFn = function(yield){ #bu/ac
  y = yield/0.01593 #kg/ha
  year = (y-1404.1)/48.3+1920
  return(year)
}
```

```{r}
round(hybridYearFn(colMeans(sc2$hybridMean)),1)
```

```{r}
round(inbredYearFn(colMeans(sc2$inbredMean)),1)
```

## References
